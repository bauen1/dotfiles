#!/usr/bin/env zsh
autoload -Uz compinit promptinit
compinit
promptinit

# This will set the default prompt to the walters theme
prompt walters

setopt PROMPT_SUBST
PROMPT='%B%F{green}%n@%m%f%F{yellow}[%D{%K:%M:%S}]%f:%F{blue}${${(%):-%~}}%f$ %b'
TMOUT=1

TRAPALRM() {
    zle reset-prompt
}

[ -r "/etc/profile" ] && emulate sh -c 'source /etc/profile'
[ -r "$HOME/.profile" ] && emulate sh -c 'source $HOME/.profile'
