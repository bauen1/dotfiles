#!/bin/sh
# support for linking / unlinking "nested" packages like
# macos/iterm2
# ~/.dotfiles:
# macos
# + iterm2
# + ...
# ...

set -e

usage() {
	echo "usage: dotfiles <cmd>"
	echo "cmd can be one of the following:"
	echo " update         update the repo"
	echo " link <pkg>     link the package to your home directory"
	echo " unlink <pkg>   remove the package from your home directory"
}

if [ $# -lt 1 ]; then
	usage
	exit 1
fi

# you should change REPO_URL
REPO_URL="https://github.com/bauen1/dotfiles.git"
REPO_DIR="$HOME/.dotfiles"
REPO_DST="$HOME"

dotfiles_git() {
	git -C "$REPO_DIR" "$@"
}

dotfiles_update() {
	if [ ! -d "$REPO_DIR" ]; then
		echo "cloning $REPO_URL to $REPO_DIR"
		git clone "$REPO_URL" "$REPO_DIR"
	else
		echo "updating $REPO_DIR"
		dotfiles_git pull
	fi
}

dotfiles_stow() {
	stow -v -t "$REPO_DST" -d "$REPO_DIR" "$@"
}

cmd="$1"
shift
case "$cmd" in
up*|update)
	dotfiles_update
	;;
l*|link)
	if [ $# -lt 1 ]; then
		echo "usage: dotfiles link <package>"
		exit 1
	fi
	dotfiles_stow --stow "$@"
	;;
un*|unlink)
	if [ $# -lt 1 ]; then
		echo "usage: dotfiles unlink <package>"
		exit 1
	fi
	dotfiles_stow --delete "$@"
	;;
r*|relink)
	if [ $# -lt 1 ]; then
		echo "usage: dotfiles relink <package>"
		exit 1;
	fi
	dotfiles_stow --restow "$@"
	;;
stow)
	dotfiles_stow "$@"
	;;
git)
	dotfiles_git "$@"
	;;
*)
	usage
	exit 1
	;;
esac
