#!/bin/bash

if [ -f ~/.profile ]; then
	# shellcheck source=.profile
	. ~/.profile
fi

# only continue if in interactive mode
[[ $- == *i* ]] || return 0

# fix everything wrong about unix ttys
stty sane

if [ -f /etc/bash_completion ]; then
	. /etc/bash_completion
fi

# ignore duplicated lines and lines starting with a space
export HISTCONTROL=ignoreboth

shopt -s histappend
# immidieatly append
PROMPT_COMMAND="history -a ; $PROMPT_COMMAND"

# a big (10mb) history
export HISTFILESIZE=-1
export HISTSIZ=-1

export HISTTIMEFORMAT='%F %T '
shopt -s cmdhist

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    if [ $(id -u) -eq 0 ]; then
        # highlight root red
        export PS1='\[\033[01;31m\]\u\[\033[01;37m\]@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\\$ '
    else
        # somebody
        export PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    fi
else
    export PS1='\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

if command -v dircolors &>/dev/null; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls -h --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vidr --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
	# TODO: colors
	SUDO_PROMPT="[sudo] password for %p@%h:"
else
    alias ls='ls -h'
	SUDO_PROMPT="[sudo] password for %p@%h:"
fi
export SUDO_PROMPT

alias ll='ls -l'
alias la='ls -A'
alias ip='ip -c'

# these will save you a lot of trouble
alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'

# source additional aliases
if [ -f ~/.bash_aliases ]; then
    # shellcheck source=.bash_aliases
    . ~/.bash_aliases
fi

# everybody likes tab-completion
# this increases load time by a lot
# [ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# use lesspipe if available
if command -v lesspipe &>/dev/null; then
	eval "$(lesspipe)"
fi

# allow for local modifications
if [ -f ~/.bashrc.local ]; then
    # shellcheck source=.bashrc.local
    . ~/.bashrc.local
fi

alias music-dl="youtube-dl -x --download-archive ~/music/youtube-dl-archive --no-overwrites -o '~/music/new_music/%(title)s-%(id)s.%(ext)s'"
