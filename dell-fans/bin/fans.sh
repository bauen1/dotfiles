#!/bin/bash
set -e

if [[ "$1" == "manual" ]]; then
	if [[ $# -lt 2 ]]; then
		printf "usage: %s manual <percent>\\n" "$0"
	else
        printf "activating manual fan control\\n"
		ipmitool raw 0x30 0x30 0x01 0x00
        printf "setting fan level to %s%%\\n" "$2"
		ipmitool raw 0x30 0x30 0x02 0xff "$2"
	fi
elif [[ "$1" == "auto" ]]; then
	printf "Activating automatic fan control\\n"
	ipmitool raw 0x30 0x30 0x01 0x01
else
	printf "usage: %s <manual|auto>\\n" "$0"
fi
