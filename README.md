# Install
 1. clone the repository to `~/.dotfiles`
    * `git clone https://gitlab.com/bauen1/dotfiles ~/.dotfiles`
 3. ensure `~/bin` exists
    * `mkdir ~/bin`
 2. link the helper script `dotfiles`
    * `~/.dotfiles/dotfiles/bin/dotfiles link dotfiles`
 3. use `dotfiles` to manage `~/.dotfiles`, for example
    * `dotfiles update` update the dotfiles repo
    * `dotfiles link <package>` to link a package to your home directory
    * `dotfiles unlink <package>` to remove a package from your home directory

# Dependencies
 * GNU Stow

## (optional) nano syntax highlighting
 * `git clone https://github.com/scopatz/nanorc .nano`
