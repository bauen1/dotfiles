" underline the current line when in insert mode (visual feedback)
if has("autocmd")
    autocmd InsertEnter,InsertLeave * set cul!
endif

" Enable Mouse support
set mouse=a

" display hidden characters
" set list

" folding
set foldmethod=syntax
set fdm=syntax

" remove trailing whitespaces
autocmd BufWritePre * %s/\s\+$//e
