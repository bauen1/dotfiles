#!/bin/sh

PATH="$HOME/bin:$HOME/.local/bin:$PATH"

LD_LIBRARY_PATH="$HOME/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH

if [ -e "$HOME/go" ]; then
	GOPATH="$HOME/go"
	export GOPATH
	PATH="$PATH:$GOPATH/bin"
fi

if [ -e "$HOME/.cargo/bin" ]; then
	PATH="$PATH:$HOME/.cargo/bin"
fi

if [ -r "$HOME/.opam/opam-init/init.sh" ]; then
    # shellcheck source=.opam/opam-init/init.sh
    . "$HOME/.opam/opam-init/init.sh"
fi

export PATH

EDITOR=nvim
export EDITOR

LIBVIRT_DEFAULT_URI="qemu:///system"
export LIBVIRT_DEFAULT_URI

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

if [ -e "$HOME/.profile.local" ]; then
	# shellcheck source=.profile.local
	. "$HOME/.profile.local"
fi

# fix ssh-agent for tmux
SSH_AUTH_SOCK="${XDG_RUNTIME_DIR:-/run/usr/$(id --user)}/openssh_agent"
export SSH_AUTH_SOCK
